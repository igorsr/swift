#include <algorithm>
#include <deque>
#include <fstream>
#include <iostream>
#include <string>

typedef std::deque<int> Lista;

Lista getListFromFile(std::string filename)
{
  Lista lista;

  std::ifstream ifs(filename.c_str());

  std::string linha;
  while (getline(ifs, linha))
    lista.push_back(std::stoi(linha));

  return lista;
}

void imprimeLista(Lista const& lista)
{
  for (auto const& elemento : lista)
    std::cout << elemento << '\n';
}

int main(int argc, char **argv)
{
  std::string filename(argv[1]);

  Lista lista = getListFromFile(filename);

  sort(lista.begin(), lista.end());

  imprimeLista(lista);
}
