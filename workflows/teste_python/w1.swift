type file;

app (file out) sortdata (file sort_script, file unsorted)
{
    python @sort_script filename(unsorted) stdout=filename(out);
}

file unsorted <"unsorted.txt">;
file sorted   <"sorted.txt">;
file sort_script <"../../app/sortfile.py">;

sorted = sortdata(sort_script, unsorted);
