type file;

app (file out) sortdata (file unsorted)
{
     csort filename(unsorted) stdout=filename(out);
}

file unsorted <"unsorted.txt">;
file sorted   <"sorted.txt">;

sorted = sortdata(unsorted);
