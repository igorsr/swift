
# Download de versão do java compatível com o Swift
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u45-b14/jdk-8u45-linux-x64.tar.gz

tar -zxvf jdk-8u45-linux-x64.tar.gz
rm jdk-8u45-linux-x64.tar.gz

# Clona repositorio com os binários, tutorial e configuração 
# de acesso ao cluster da PGMC
wget https://bitbucket.org/igorsr/swift/downloads/swift.tar.gz

tar -zxvf swift.tar.gz
rm swift.tar.gz
